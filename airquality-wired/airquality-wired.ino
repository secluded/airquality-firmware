/*
   Air Quality Monitoring System
   Written for Gold Coast TechSpace Hackergrant II project
   ©2016 Steve Dalton
   https://bitbucket.org/secluded/airquality-firmware
*/

#include <SPI.h>
#include <Ethernet.h>
#include <PubSubClient.h>
#include "LedControl.h"
#include "secrets.h"

#define PIN_DIN 5
#define PIN_LOAD 3
#define PIN_CLK 4

#define PIN_BUTTON 2

#define DELAY_TIME 250

int count = 0;

byte mac[]    = {  0xDE, 0xED, 0xBA, 0xFE, 0xFE, 0xED };

IPAddress ip(192, 168, 111, 143);
IPAddress server(54, 79, 0, 48);
LedControl lc = LedControl(PIN_DIN, PIN_CLK, PIN_LOAD, 2);

void callback(char* topic, byte* payload, unsigned int length) {}

EthernetClient ethClient;
PubSubClient client(ethClient);

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("airQualityClient", MQTT_USER, MQTT_PASSWORD)) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("announce", "hello world");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void initNetwork() {
  Serial.println("Initialising Network");

  client.setServer(server, 1883);
  client.setCallback(callback);

  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    // try to congifure using IP address instead of DHCP:
    Ethernet.begin(mac, ip);
  }
  printIPAddress();

  // Allow the hardware to sort itself out
  delay(1500);

  Serial.println("Network up");
}

void initScreen() {
  Serial.println("Initialising Screen");

  lc.shutdown(0, false);
  /* Set the brightness to a medium values */
  lc.setIntensity(0, 4);
  lc.setIntensity(1, 4);
}

void setup() {
  Serial.begin(115200);

  pinMode(PIN_BUTTON, INPUT_PULLUP);

  Serial.println("Starting");

  initScreen();
  showWelcomeMessage();
  initNetwork();

  for (int i = 0; i < 5; i++) {
    Serial.println("Warming Up");
    showWarmUpMessage();
    delay(1000);
  }

  lc.clearDisplay(0);

  attachInterrupt(digitalPinToInterrupt(PIN_BUTTON), buttonPressed, FALLING);
}

void buttonPressed() {
  Serial.println("Interupt");
  lc.shutdown(0, false);
  count = 0;
}

void loop() {
  count++;

  if (!client.connected()) {
    reconnect();
  }

  int c02value = readC02();
  char buffer[12];
  itoa(c02value, buffer, 10);

  client.publish("/aq", buffer);
  showValueOnScreen(c02value);

  delay(5000);
  if (count >= 10) {
    lc.shutdown(0, true); //  power saving
  }
}

void showWelcomeMessage() {
  lc.clearDisplay(0);
  lc.setChar(0, 7, 'H', false);
  delay(DELAY_TIME);
  lc.setChar(0, 6, 'E', false);
  delay(DELAY_TIME);
  lc.setChar(0, 5, 'L', false);
  delay(DELAY_TIME);
  lc.setChar(0, 4, 'L', false);
  delay(DELAY_TIME);
  lc.setChar(0, 3, '0', true);
  delay(DELAY_TIME);
  lc.setRow(0, 2, B01001110); // Upper case C
  delay(DELAY_TIME);
  lc.setChar(0, 1, 'o', false);
  delay(DELAY_TIME);
  lc.setChar(0, 0, '2', false);
  delay(DELAY_TIME);
}

void showWarmUpMessage() {
  lc.clearDisplay(0);
  for (int i = 7; i >= 0; i--) {
    lc.setChar(0, i, '.', true);
    delay(DELAY_TIME);
  }
}

void showValueOnScreen(int reading) {
  Serial.print(reading);
  Serial.print(": ");

  boolean negative = reading < 0;
  reading = abs(reading);
  int tenthousands=reading/10000;
  int thousands=reading%10000/1000;
  int hundreds=reading%1000/100;
  int tens=reading%100/10;
  int ones=reading%10;

  if(negative) Serial.print('-'); Serial.print(' ');
  Serial.print(tenthousands); Serial.print(' ');
  Serial.print(thousands); Serial.print(' ');
  Serial.print(hundreds); Serial.print(' ');
  Serial.print(tens); Serial.print(' ');
  Serial.println(ones);

  lc.setChar(0, 7, negative ? '-' : ' ', false);
  lc.setRow(0, 6, 0);
  lc.setRow(0, 5, 0);
  lc.setDigit(0, 4, tenthousands, false);
  lc.setDigit(0, 3, thousands, false);
  lc.setDigit(0, 2, hundreds, false);
  lc.setDigit(0, 1, tens, false);
  lc.setDigit(0, 0, ones, false);
}

void printIPAddress()
{
  Serial.print("My IP address: ");
  for (byte thisByte = 0; thisByte < 4; thisByte++) {
    // print the value of each byte of the IP address:
    Serial.print(Ethernet.localIP()[thisByte], DEC);
    Serial.print(".");
  }

  Serial.println();
}


